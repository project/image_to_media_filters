Image to Media Filters for Drupal 8 and 9
=====

A Drupal 8 and 9 module which fixes legacy code such as:

[img_assist|nid=9491|title=|desc=|link=none|align=right|width=100|height=37]
[acidfree:9491 align=left size=80x163]
[image:9491]

In this example, a node with nid 9491 contains a media reference to an image, in a field called field_image_media.

I created this module because I needed it for migration of my site, which used (at different times) the acidfree, image_filter, and img_assist modules to embed image tags in text fields.

Usage
-----

You can install this module as any other Drupal 8 or 9 module and it will add three text filters to your drupal system, called:

* Convert acidfree Image Tags to Media
* Convert image_filter Image Tags to Media
* Convert img_assist Image Tags to Media

image nodes must exist
-----

This module doesn't care what kind of nodes your image references are attached to, but it does expect that the fields are attached to nodes, and that they are called field_image_media. A quick edit to the module will support other field names. I may add field selection in the future.


conversion in place
-----

If you would rather rewrite the contents of the database so you don't need these filters, then you can easily achieve this using drush:

Step 0: *back up your database*

Step 0.5: optionally edit your node properties and enable new revisions, which will let you revert the changes later.

Step 1: preview:
drush ev "Drupal\\image_to_media_filters\\ImageToMediaFilters\\DbReplacer::instance()->replaceAll('node', 'book', '*filter*', TRUE);" | less
(the TRUE means *simulate*)

Step 2: process:
drush ev "Drupal\\image_to_media_filters\\ImageToMediaFilters\\DbReplacer::instance()->replaceAll('node', 'book', '*filter*', FALSE);" | tee /tmp/filter.log


Resources
-----

* See https://www.drupal.org/project/convert_media_tags_to_markup

Drupal 9 readiness
-----

This project is Drupal 9 ready.
