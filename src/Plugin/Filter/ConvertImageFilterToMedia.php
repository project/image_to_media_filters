<?php

namespace Drupal\image_to_media_filters\Plugin\Filter;

use Drupal\image_to_media_filters\ImageToMediaFilters\App;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Provides a filter for converting legacy media tags to markup.
 *
 * See ./README.md for details.
 * This code is adapted from
 * http://cgit.drupalcode.org/media/tree/modules/media_wysiwyg/includes/media_wysiwyg.filter.inc?h=7.x-3.x.
 *
 * @Filter(
 *   id = "convert_image_filter_tags_to_media",
 *   module = "image_to_media_filters",
 *   title = @Translation("Convert image_filter Image Tags to Media"),
 *   description = @Translation("Renders Drupal 6 image_filter tags as Media Embed tags."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE,
 * )
 */
class ConvertImageFilterToMedia extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    try {
      return new FilterProcessResult(App::instance()->filterText($text, 'image_filter'));
    }
    catch (\Exception $e) {
      $this->watchdogException($e);
      return new FilterProcessResult($text);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    return '<p>Converts legacy image_filter image tags to media image tags.</p>';
  }
}
