<?php

namespace Drupal\image_to_media_filters\Plugin\Filter;

use Drupal\image_to_media_filters\ImageToMediaFilters\App;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Provides a filter for converting legacy media tags to markup.
 *
 * See ./README.md for details.
 * This code is adapted from
 * http://cgit.drupalcode.org/media/tree/modules/media_wysiwyg/includes/media_wysiwyg.filter.inc?h=7.x-3.x.
 *
 * @Filter(
 *   id = "convert_img_assist_tags_to_media",
 *   module = "image_to_media_filters",
 *   title = @Translation("Convert img_assist Image Tags to Media"),
 *   description = @Translation("Renders Drupal 6 img_assist tags as Media Embed tags."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE,
 * )
 */
class ConvertImgAssistToMedia extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    try {
      return new FilterProcessResult(App::instance()->filterText($text, 'acidfree'));
    }
    catch (\Exception $e) {
      $this->watchdogException($e);
      return new FilterProcessResult($text);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    return '<p>Converts legacy img_assist image tags to media image tags.</p>';
  }

}
