<?php

namespace Drupal\image_to_media_filters\ImageToMediaFilters;

use Drupal\image_to_media_filters\traits\Singleton;
use Drupal\image_to_media_filters\traits\CommonUtilities;

class App {

  use Singleton;
  use CommonUtilities;

  /**
   * Filter text.
   *
   * The is the heart of this module: change media tags code to an actual
   * image tag. This can be done by the filter plugin or by the DbReplacer
   * object.
   *
   * @param string $text
   *   The text to filter.
   * @param string $filter
   *   The name of the module which supported the original filter
   * @return string
   *   The filtered text.
   *
   * @throws Exception
   */
  public function filterText(string $text, string $filter) : string {
    $regex_token['img_assist'] = '/\[img_assist\|.*?\]/s';
    $regex_token['acidfree'] = '/\[acidfree\:.*?\]/s';
    $regex_token['image_filter'] = '/\[image\:.*?\]/s';

    if (!in_array($filter, array_keys($regex_token)))
      throw new \Exception('No such filter ' . $filter);

    $rendered_text = $text;
    $count = 1;
    preg_match_all($regex_token[$filter], $text, $matches);
    if (!empty($matches[0])) {
      foreach ($matches[0] as $match) {
        $replacement = $this->tokenToMarkup([$match], $filter, FALSE);
        $rendered_text = str_replace($match, $replacement, $rendered_text, $count);
      }
    }
    return $rendered_text;
  }

  /**
   * Replace callback to convert an image tag into media embed markup.
   *
   * This code is adapted from
   * http://cgit.drupalcode.org/media/tree/modules/media_wysiwyg/includes/media_wysiwyg.filter.inc?h=7.x-3.x.
   *
   * @param string $match
   *   Takes a match of tag code.
   * @param string $filter
   *   The name of the module which supported the original filter
   * @param bool $wysiwyg
   *   Set to TRUE if called from within the WYSIWYG text area editor.
   *
   * @return string
   *   The HTML markup representation of the tag, or an empty string on failure.
   */
  public function tokenToMarkup($match, $filter, $wysiwyg = FALSE) {
    try {
      switch ($filter) 
      {
	case 'img_assist':
          $match = str_replace('[img_assist|', '', $match);
          $match = str_replace(']', '', $match);
          $delim[0] = '|';
	  $delim[1] = '=';
	  break;
	case 'acidfree':
          $match = str_replace('[acidfree:', 'nid=', $match);
          $match = str_replace(']', '', $match);
          $delim[0] = ' ';
	  $delim[1] = '=';
	  break;
	case 'image_filter':
          $match = str_replace('[image:', 'nid=', $match);
          $match = str_replace(']', '', $match);
          $delim[0] = ' ';
	  $delim[1] = '=';
	  break;
    }

      $tag = $match[0];
      if (!is_string($tag)) {
        throw new \Exception('Unable to find matching tag');
      }

      foreach (explode($delim[0], $tag) as $tagelement) {
	$tagelements = explode($delim[1], $tagelement);
	$tag_info[$tagelements[0]] = empty($tagelements[1]) ? '' : $tagelements[1];
      } 

      if (!isset($tag_info['nid'])) {
        throw new \Exception('No node ID');
      }

      $node = \Drupal\node\Entity\Node::load($tag_info['nid']);

      if (is_null($node)) {
	throw new \Exception('Could not load node, nid=' . $tag_info['nid']);
      }

      if($node->hasField('field_image_media')){
	$image_field = $node->get('field_image_media');
        if ($image_field->isEmpty())
          throw new \Exception('media field on image node is empty');
	$media_entity = $image_field->referencedEntities()[0];
	// my attempts to test whether I have a valid media entity have so far failed
        //if ($media_entity instanceof Drupal\media\MediaInterface) {
	  $uuid = $media_entity->uuid();
	//} else {
	//  throw new \Exception('media entity somehow is not media');
	//}
      }

      /*
       * this is where the properties are configured
       *
       * you can provide a default value after the ? operator
       */

      $align = empty($tag_info['align']) ? 'center' : $tag_info['align'];
      $title = empty($tag_info['title']) ? '' : $tag_info['title'];
      $alt = empty($tag_info['desc']) ? ( empty($title) ? '' : $title ) : $tag_info['desc'];

      // currently unused variables - style should be a thing though
      //$height = empty($tag_info['height']) ? '' : 'height="' . $tag_info['height'] . '"';
      //$width = empty($tag_info['width']) ? '' : 'width="' . $tag_info['width'] . '"';
      //$class = empty($tag_info['class']) ? '' : $tag_info['class'];
      //$style = empty($tag_info['style']) ? '' : $tag_info['style'];
      $output = '<drupal-media data-entity-type="media" data-entity-uuid="' . $uuid . '"' .
	      ( $align ? ( ' data-align="' . $align . '"' ) : '' ) .
	      ( $alt ? ( ' alt="' . $alt . '"' ) : '' ) .
	      ( $title ? ( ' data-caption="' . $title . '"' ) : '' ) .
	      '>&nbsp;</drupal-media>';
      return $output;
    }
    catch (\Throwable $t) {
      $this->watchdogThrowable($t);
      return '';
    }
  }
}
