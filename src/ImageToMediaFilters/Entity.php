<?php

namespace Drupal\image_to_media_filters\ImageToMediaFilters;

use Drupal\image_to_media_filters\traits\CommonUtilities;
use Drupal\Core\Entity\EntityInterface as DrupalEntity;

/**
 * Represents a Drupal entity for our purposes.
 */
class Entity {

  use CommonUtilities;

  /**
   * Constructor.
   *
   * @param Drupal\Core\Entity\EntityInterface $entity
   *   The Drupal entity for which this object is a wrapper.
   */
  public function __construct(DrupalEntity $entity) {
    $this->entity = $entity;
  }

  /**
   * Process this entity; change the media tags code to an image tag.
   *
   * @param string $filter
   *   Which filter to apply
   * @param bool $simulate
   *   Whether or not to Simulate the results.
   * @param string $log
   *   For example "print_r" or "dpm".
   *
   * @throws Exception
   */
  public function process(string $filter, bool $simulate = TRUE, string $log = 'print_r') {
    foreach ($this->entity->getFields() as $fieldname => $field) {
      $this->processField($fieldname, $field, $filter, $simulate, $log);
    }
    if ($simulate) {
      $log('Not actually saving entity ' . $this->entity->id() . ' because we are in simulation mode.' . PHP_EOL);
    }
    else {
      $log('Saving entity ' . $this->entity->id() . ' because we are not in simulation mode.' . PHP_EOL);
      $this->entity->save();
    }
  }

  /**
   * Process a field.
   *
   * @param string $fieldname
   *   A field name.
   * @param object $field
   *   A field list object.
   * @param string $filter
   *   Which filter to apply
   * @param bool $simulate
   *   Whether or not to Simulate the results.
   * @param string $log
   *   For example "print_r" or "dpm".
   *
   * @throws Exception
   */
  public function processField(string $fieldname, $field, string $filter, bool $simulate = TRUE, $log = 'print_r') {
    $log('Processing field ' . $fieldname . ' of class ' . get_class($field) . ' with filter ' . $filter . ' for entity ' . $this->entity->id() . PHP_EOL);
    $value = $field->getValue();
    foreach ($value as $delta => $row) {
      if (!empty($row['value']) && !empty($row['format'])) {
        $log(' => Item at position ' . $delta . ' is a candidate for processing' . PHP_EOL);
	$value[$delta]['value'] = App::instance()->filterText($row['value'], $filter);
        if ($simulate) {
          $log('Simulating changing the content to: ' . PHP_EOL);
          $log($value[$delta]['value']);
          $log(PHP_EOL);
        }
        else {
          $this->entity->{$fieldname} = $value;
          $log('Changed its content.' . PHP_EOL);
        }
      }
      else {
        $log(' => Item at position ' . $delta . ' is not a candidate for processing' . PHP_EOL);
      }
    }
  }
}
